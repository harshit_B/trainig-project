public class Student {
String name;
int age;
int marks;

    public Student(String name) {
        this.name=name;
    }

    public Student(String name ,int age) {
        this(name);
        this.age = age;
    }

    public Student(String name, int marks,int age) {
        this(name,age);
        this.marks=marks;

    }
}
